import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import Store from '@store/index';
import { Application } from './components';

import 'reset-css/reset.css';
import '@Styles/index.scss';

ReactDOM.render(
	<Provider store = { Store }>
		<Application />
	</Provider>,
	document.getElementById( 'root' ),
);

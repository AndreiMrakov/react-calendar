import { combineReducers } from 'redux';
import commonReducer, { commonReducerType } from './common';

export type ReducerType = {
	commonReducer: commonReducerType
};

export default combineReducers( {
	commonReducer,
} );

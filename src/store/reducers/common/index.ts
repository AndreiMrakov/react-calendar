import { common } from '@store/actionTypes';
import { actionType } from '@store/types';

export type commonReducerType = {
	loading: boolean,
	isDarkMode: boolean
};

const initialState = {
	loading: true,
	isDarkMode: false
};

export default ( state = initialState, { type, payload }: actionType ): commonReducerType => {
	switch ( type ) {
		case common.SET_IS_AUTH_LOADING: {
			return { ...state, loading: payload };
		}
		case common.TRIGGER_THEME_DARK_MODE: {
			return { ...state, isDarkMode: payload };
		}
		default :
			return state;
	}
}

import { common } from '../actionTypes';

export type SetIsAuthLoadingType = {
	type: typeof common.SET_IS_AUTH_LOADING,
	payload: boolean
};

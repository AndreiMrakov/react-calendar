import React from 'react';

import { CalendarProps } from './types';
import { viewModesType } from './utils/constants';
import { ChangeDateButton, Month } from './components';

import styles from './styles.module.scss';

const Calendar: React.FC<CalendarProps> = ( { date } ) => {
	const type: viewModesType = 'month';
	return (
		<div className = { styles['container'] }>
			<div className = { styles['header'] }>
				header
			</div>
			<div className = { styles['body'] }>
				<ChangeDateButton direction = { 'left' } />
				<Month />
				<ChangeDateButton direction = { 'right' } />
			</div>
			<div className = { styles['footer'] }>
				footer
			</div>
		</div>
	);
};

export { Calendar };

import React from 'react';
import styles from './styles.module.scss';

export const Month = ( { type = 'month' } ) => {
	return (
		<div className = { styles[type] }>
			<div className = "week-days">
				week days
			</div>
			<div className = "month-days">
				month days
			</div>
		</div>
	);
};

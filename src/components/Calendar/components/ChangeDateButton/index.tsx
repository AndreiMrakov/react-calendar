import React from 'react';

import styles from './styles.module.scss';
import classNames from 'classnames';

export const ChangeDateButton = ( { direction = 'left' } ) => {
	return (
		<div className = { classNames( styles['arrow'], styles[direction] ) }>
			{
				direction === 'left'
					? '<'
					: '>'
			}
		</div>
	);
};

import React from 'react';

import styles from './styles.module.scss';
import { OldCalendar } from '@src/components';
import { Calendar } from '@src/components/Calendar';

const Application: React.FC = () => {
	return (
		<>
			<div className = { styles.container }>
				<OldCalendar />
			</div>
			<div className = { styles.container }>
				<Calendar />
			</div>
		</>
	);
};

export default Application;

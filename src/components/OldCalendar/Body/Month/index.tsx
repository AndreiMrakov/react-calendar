import moment from "moment";
import React from "react";
import { ChangeMonthButton } from "@src/components/OldCalendar/Body/ChangeDateButton";
import Context from "@src/components/OldCalendar/utils/Context";
import DayCell from "@src/components/OldCalendar/Body/Month/DayCell";

import style from './style.module.scss';
import Week from "@src/components/OldCalendar/Body/Month/WeekDays";

const Month = ( { ...props } ) => (
	<Context.Consumer>
		{
			( { date, setDateAndPeriod, ...cProps } ) => {
				const daysOfMonthCreator = date => {
					const CALENDAR_DAYS_NUMBER = 42;
					const dayOfMonthGenerator = date => {
						return Array.from(' '.repeat( moment( date ).daysInMonth()))
							.map( (day,index) => ( { value: moment( moment( date ).date( index + 1 )).format(), isCurrentMonth: true } ) );
					};
					let allDayCells = dayOfMonthGenerator( date );
					let firstMonthDayCellWeekDayCell = moment( moment( date ).date( 1 ).format() ).isoWeekday();

					if ( firstMonthDayCellWeekDayCell !== 1 ) {
						let previousMonthDate = moment( date ).subtract( 1, "month" ).format();
						allDayCells = [
							...dayOfMonthGenerator( previousMonthDate ).slice( 1 - firstMonthDayCellWeekDayCell ).map( day => ( {
								...day,
								isCurrentMonth: false,
							} ) ), ...allDayCells,
						];
					}

					if ( allDayCells.length < CALENDAR_DAYS_NUMBER ) {
						let nextMonthDate = moment( date ).add( 1, "month" ).format();
						allDayCells = [
							...allDayCells,
							...dayOfMonthGenerator( nextMonthDate )
								.slice( 0, CALENDAR_DAYS_NUMBER - allDayCells.length )
								.map( day => ( { ...day, isCurrentMonth: false } ) ),
						];
					}

					return allDayCells;
				};
				const handleChangeMonth = newDate => {
					if ( moment( newDate ).isValid() ) {
						setDateAndPeriod( { date: newDate } );
					}
				};
				return (
					<div className = { style["month-container"] }>
						<ChangeMonthButton direction = { "left" } onClick = { handleChangeMonth } date = { date } />
						<Week />
						<div className = { style["month"] }>
							{
								daysOfMonthCreator( date ).map( ( day, index ) =>{
									return <DayCell
										key = { index }
										dayDate = { day }
										onClick = { cProps.selectDate }
										selectedDate = { cProps.selectedDate }
										setHoverDate = { cProps.setHoverDate }
										hoveredDate = { cProps.hoveredDate }
										setSelectedHoveredDate = { cProps.setSelectedHoveredDate }
										selectedHoveredDate = { cProps.selectedHoveredDate }
										invalidPopup={cProps.invalidPopup}
									/>},
								)
							}
						</div>
						<ChangeMonthButton direction = { "right" } onClick = { handleChangeMonth } date = { date } />
					</div>
				);
			}
		}
	</Context.Consumer>
);

export default Month;

import moment from "moment";
import React from "react";

import style from './style.module.scss';

const Week = ( { date, ...props } ) => {
	const weekdaySimpleFn = [ ...[ ...moment.weekdaysShort() ].slice( 1 ), moment.weekdaysShort()[0] ]
		.map( ( day, index ) => ( { index: index + 1, value: day } ) );

	return (
		<div className = { style["week-days"] }>
			{
				weekdaySimpleFn.map( day =>
					<div
						className = { `${ style["week-day"] } ${ day.index > 5 ? style["weekend"] : "" }` }
						key = { day.index }
					>
						{ day.value }
					</div> )
			}
		</div>
	);
};

export default Week;

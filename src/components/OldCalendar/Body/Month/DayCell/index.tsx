import moment from 'moment';
import React from 'react';
import IncorrectDateTooltip from '@src/components/OldCalendar/IncorrectDateTooltip';

import style from './style.module.scss';

const DayCell = ( { dayDate, onClick, setHoverDate, hoveredDate, selectedHoveredDate, ...props } ) => {
	const [ isOpen, setIsOpen ] = React.useState( false );
	React.useEffect( () => {
		if ( selectedHoveredDate && moment( dayDate.value ).isSame( selectedHoveredDate, 'day' ) ) {
			setIsOpen( true );
		} else if ( isOpen ) {
			setIsOpen( false );
		}
	}, [ selectedHoveredDate ] );
	const handleClick = e => {
		e.stopPropagation();
		if ( typeof onClick === 'function' ) {
			if ( hoveredDate && moment( dayDate.value ).isSame( hoveredDate.value, 'day' ) && !hoveredDate.valid ) {
				props.setSelectedHoveredDate( dayDate.value );
			} else {
				onClick( dayDate.value );
			}
		}
	};
	const onHoverOn = () => {
		if ( typeof onClick === 'function' ) {
			setHoverDate( dayDate.value, 'day' );
		}
	};
	const onHoverOff = () => {
		if ( typeof onClick === 'function' ) {
			setHoverDate( null );
		}
	};

	let className = `${ style['day'] }` +
		`${ !dayDate.isCurrentMonth ? ` ${ style['not-current-month'] }` : '' }` +
		`${ moment( dayDate.value ).isSame( moment(), 'day' ) ? ` ${ style['today'] }` : '' }` +
		`${ moment( dayDate.value ).isoWeekday() > 5 ? ` ${ style['weekend'] }` : '' }` +
		`${ moment( dayDate.value ).isSame( props.selectedDate, 'day' ) ? ` ${ style['selected'] }` : '' }` +
		`${ hoveredDate.value && moment( dayDate.value ).isSame( hoveredDate.value, 'day' )
			? !hoveredDate.valid
				? ` ${ style['invalid'] }`
				: ` ${ style['hovered'] }`
			: '' }` +
		`${ isOpen ? ` ${ style['invalid'] }` : '' }`;

	return (
		<IncorrectDateTooltip
			open = { isOpen }
			end = { props.invalidPopup === 'end' }
			trigger = {
				<div
					className = { className }
					onClick = { handleClick }
					onMouseEnter = { onHoverOn }
					onMouseLeave = { onHoverOff }
				>
					{ moment( dayDate.value ).format( 'D' ) }
				</div>
			}
		/>
	);
};

export default DayCell;

import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import moment from 'moment';
import React from 'react';

import { MAX_DATE, MIN_DATE } from '@src/components/OldCalendar/utils/constants';

import style from './style.module.scss';

const ChangeDateButton = ( { date, direction = 'right', onClick, moveRange, ...props }: { date: string, direction: string, onClick: ( str: string ) => null, moveRange: string } ) => {
	const handleClick = () => {
		if ( typeof onClick === 'function' && moment( date ).isValid() && moveRange ) {
			if ( moveRange !== 'years' ) {
				onClick( moment( date )[direction === 'left' ? 'subtract' : 'add']( 1, moveRange ).format() );
			} else {
				onClick( moment( date )[direction === 'left' ? 'subtract' : 'add']( 12, 'year' ).format() );
			}
		}
	};
	const isDisabled = () => {
		if ( moveRange !== 'years' ) {
			if ( direction === 'left' && moment( date ).subtract( 1, moveRange ).isBefore( MIN_DATE, 'year' ) ) {
				return true;
			} else return direction === 'right' && moment( date ).add( 1, moveRange ).isAfter( MAX_DATE, 'year' );
		} else {
			if ( direction === 'left' && moment( date ).subtract( 12, 'year' ).isBefore( MIN_DATE, 'year' ) ) {
				return true;
			} else return direction === 'right' && moment( date ).add( 12, 'year' ).isAfter( MAX_DATE, 'year' );
		}
	};

	return (
		<div
			className = { `${ style['arrow'] } ${ direction === 'right' ? style['right'] : style['left'] } ${ isDisabled()
				? style['disabled']
				: '' }` }
			onClick = { handleClick }
		>
			<i className = { style['icon'] }>
				{
					direction === 'right'
						? <FontAwesomeIcon icon = { faChevronRight } />
						: <FontAwesomeIcon icon = { faChevronLeft } />
				}
			</i>
		</div>
	);
};

export default ChangeDateButton;

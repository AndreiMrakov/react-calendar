import React from "react";
import ChangeDateButton from "./ChangeDateButton";

export const ChangeMonthButton = props => <ChangeDateButton moveRange = { "month" } { ...props } />;
export const ChangeYearButton = props => <ChangeDateButton moveRange = { "year" } { ...props } />;
export const ChangeYearsRange = props => <ChangeDateButton moveRange = { "years" } { ...props } />;
export default ChangeDateButton;

import moment from 'moment';
import React from 'react';
import { MAX_DATE, MIN_DATE } from '@src/components/OldCalendar/utils/constants.js';
import Context from '@src/components/OldCalendar/utils/Context';
import { ChangeYearsRange } from '@src/components/OldCalendar/Body/ChangeDateButton';

import style from './style.module.scss';
import YearCell from '@src/components/OldCalendar/Body/Years/YearCell';

const Years = ( { ...props } ) => {
	return (
		<Context.Consumer>
			{
				( { date, setDateAndPeriod, delta, ...cProps } ) => {
					const yearsRangeGenerator = () => {
						return Array.from(' '.repeat( 12 ))
							.map( (year,index) => moment( moment().add( delta * 12, 'years' ) )
								.add( index - 4, 'years' ).format() )
							.map( year => moment( year ).isBetween( MIN_DATE, MAX_DATE, '[]' ) ? year : '' );
					};

					const handleChangeYears = newDate => {
						if ( moment( newDate ).isValid() ) {
							setDateAndPeriod( { date: newDate } );
						}
					};
					const handleSetYear = year => {
						if ( moment( date ).isValid() && moment( date ).year( year ).isValid() ) {
							setDateAndPeriod( { date: moment( date ).year( year ).format(), period: 'year' } );
						}
					};
					return (
						<div className = { style['years-container'] }>
							<ChangeYearsRange direction = { 'left' } onClick = { handleChangeYears } date = { date } />
							<div className = { style['years'] }>
								{
									yearsRangeGenerator().map( ( year, index ) =>
										<YearCell
											key = { index }
											yearDate = { year }
											onClick = { handleSetYear }
											selectedDate = { cProps.selectedDate }
											setHoverDate = { cProps.setHoverDate }
											hoveredDate = { cProps.hoveredDate }
											selectedHoveredDate = { cProps.selectedHoveredDate }
											setSelectedHoveredDate = { cProps.setSelectedHoveredDate }
											invalidPopup = { cProps.invalidPopup }
										/>,
									)
								}
							</div>
							<ChangeYearsRange direction = { 'right' } onClick = { handleChangeYears } date = { date } />
						</div>
					);
				}
			}
		</Context.Consumer>
	);
};

export default Years;

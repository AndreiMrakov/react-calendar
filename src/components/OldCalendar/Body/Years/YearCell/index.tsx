import moment from 'moment';
import React from 'react';
import IncorrectDateTooltip from '@src/components/OldCalendar/IncorrectDateTooltip';

import style from './style.module.scss';

const YearCell = ( { yearDate, period, onClick, selectedDate, setHoverDate, hoveredDate, selectedHoveredDate, ...props } ) => {
	const [ isOpen, setIsOpen ] = React.useState( false );
	React.useEffect( () => {
		if ( selectedHoveredDate && moment( yearDate ).isSame( selectedHoveredDate, 'month' ) ) {
			setIsOpen( true );
		} else if ( isOpen ) {
			setIsOpen( false );
		}
	}, [ selectedHoveredDate ] );
	const handleClick = e => {
		e.stopPropagation();
		if ( typeof onClick === 'function' ) {
			if ( hoveredDate && moment( yearDate ).isSame( hoveredDate.value, 'year' ) && !hoveredDate.valid ) {
				props.setSelectedHoveredDate( yearDate );
			} else {
				// } else if ( !moment(monthDate).isSame( selectedDate, "month" ) ) {
				onClick( moment( yearDate ).format( 'YYYY' ) );
			}
		}
	};

	const onHoverOn = () => {
		if ( typeof setHoverDate === 'function' ) {
			setHoverDate( yearDate, 'year' );
		}
	};
	const onHoverOff = () => {
		if ( typeof setHoverDate === 'function' ) {
			setHoverDate( null, 'year' );
		}
	};

	let className = `${ style['year'] }` +
		`${ moment( yearDate ).isSame( moment(), 'year' ) ? ` ${ style['current'] }` : '' }` +
		`${ moment( yearDate ).isSame( moment( selectedDate ), 'year' ) ? ` ${ style['selected'] }` : '' }` +
		`${ hoveredDate.value && moment( yearDate ).isSame( hoveredDate.value, 'year' )
			? !hoveredDate.valid
				? ` ${ style['invalid'] }`
				: ` ${ style['hovered'] }`
			: '' }` +
		`${ isOpen ? ` ${ style['invalid'] }` : '' }`;

	return (
		<IncorrectDateTooltip
			open = { isOpen }
			end = { props.invalidPopup === 'end' }
			trigger = {
				<div className = { className }>
					<p
						className = { `${ style['value'] }${ !yearDate ? ` ${ style['empty'] }` : '' }` }
						onClick = { yearDate ? handleClick : null }
						onMouseEnter = { yearDate ? onHoverOn : null }
						onMouseLeave = { yearDate ? onHoverOff : null }
					>
						{ yearDate && moment( yearDate ).format( 'YYYY' ) }
					</p>
				</div>
			}
		/>
	);
};
export default YearCell;

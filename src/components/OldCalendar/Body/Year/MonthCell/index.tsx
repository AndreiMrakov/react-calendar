import moment from 'moment';
import React from 'react';
import IncorrectDateTooltip from '@src/components/OldCalendar/IncorrectDateTooltip';

import style from './style.module.scss';

const MonthCell = ( { monthDate, onClick, hoveredDate, setHoverDate, selectedHoveredDate, ...props } ) => {
	const [ isOpen, setIsOpen ] = React.useState( false );
	React.useEffect( () => {
		if ( selectedHoveredDate && moment( monthDate ).isSame( selectedHoveredDate, 'month' ) ) {
			setIsOpen( true );
		} else if ( isOpen ) {
			setIsOpen( false );
		}
	}, [ selectedHoveredDate ] );
	const handleChangeMonth = e => {
		e.stopPropagation();
		if ( typeof onClick === 'function' ) {
			if ( hoveredDate && moment( monthDate ).isSame( hoveredDate.value, 'month' ) && !hoveredDate.valid ) {
				props.setSelectedHoveredDate( monthDate );
			} else {
				// } else if ( !moment(monthDate).isSame( selectedDate, "month" ) ) {
				onClick( { date: moment( monthDate ).format(), period: 'month' } );
			}
		}
	};

	const onHoverOn = () => {
		if ( typeof setHoverDate === 'function' ) {
			setHoverDate( monthDate, 'month' );
		}
	};
	const onHoverOff = () => {
		if ( typeof setHoverDate === 'function' ) {
			setHoverDate( null );
		}
	};

	let className = `${ style['month'] }` +
		`${ moment( monthDate ).isSame( moment(), 'month' ) ? ` ${ style['current'] }` : '' }` +
		`${ moment( monthDate ).isSame( moment( props.selectedDate ), 'month' ) ? ` ${ style['selected'] }` : '' }` +
		`${ hoveredDate.value && moment( monthDate ).isSame( hoveredDate.value, 'month' )
			? !hoveredDate.valid
				? ` ${ style['invalid'] }`
				: ` ${ style['hovered'] }`
			: '' }` +
		`${ isOpen ? ` ${ style['invalid'] }` : '' }`;
	return (
		<IncorrectDateTooltip
			open = { isOpen }
			end = { props.invalidPopup === 'end' }
			trigger = {
				<div className = { className }>
					<p
						className = { `${ style['value'] }` }
						onClick = { handleChangeMonth }
						onMouseEnter = { onHoverOn }
						onMouseLeave = { onHoverOff }
					>
						{ moment( monthDate ).format( 'MMM' ) }
					</p>
				</div>
			}
		/>
	);
};
export default MonthCell;

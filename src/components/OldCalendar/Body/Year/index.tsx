import moment from 'moment';
import React from 'react';
import { ChangeYearButton } from '@src/components/OldCalendar/Body/ChangeDateButton';
import Context from '@src/components/OldCalendar/utils/Context';
import MonthCell from '@src/components/OldCalendar/Body/Year/MonthCell';

import style from './style.module.scss';

const Year = ( { ...props } ) => {
	return (
		<Context.Consumer>
			{
				( { date, setDateAndPeriod, ...cProps } ) => {
					let monthInYear = moment.months().map( ( month, index ) => moment( date ).month( index ).format() );

					const handleChangeYear = newDate => {
						if ( moment( newDate ).isValid() ) {
							setDateAndPeriod( { date: newDate } );
						}
					};
					return (
						<div className = { style['months-container'] }>
							<ChangeYearButton direction = { 'left' } onClick = { handleChangeYear } date = { date } />
							<div className = { style['months'] }>
								{
									monthInYear.map( month =>
										<MonthCell
											monthDate = { month }
											key = { month }
											selectedDate = { cProps.selectedDate }
											onClick = { setDateAndPeriod }
											setHoverDate = { cProps.setHoverDate }
											hoveredDate = { cProps.hoveredDate }
											selectedHoveredDate = { cProps.selectedHoveredDate }
											setSelectedHoveredDate = { cProps.setSelectedHoveredDate }
											invalidPopup = { cProps.invalidPopup }
										/> )
								}
							</div>
							<ChangeYearButton direction = { 'right' } onClick = { handleChangeYear } date = { date } />
						</div>
					);
				}
			}
		</Context.Consumer>
	);
};

export default Year;

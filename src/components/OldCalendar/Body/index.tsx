import React from "react";
import Month from "@src/components/OldCalendar/Body/Month";

import style from './style.module.scss';
import Year from "@src/components/OldCalendar/Body/Year";
import Years from "@src/components/OldCalendar/Body/Years";

const Body = ( { period, ...props } ) => {
	const modeReturner = period => {
		switch ( period ) {
			case "year":
				return <Year />;
			case "years":
				return <Years />;
			default :
				return <Month />;
		}
	};
	return (
		<div className = { style["calendar-container"] }>
			{
				modeReturner( period )
			}
		</div>
	);
};
export default Body;

import moment from "moment";

export const MIN_DATE = moment().isValid()
	? moment().subtract( 100, "years" ).month( 0 ).date( 1 ).hours( 0 ).minutes( 0 ).seconds( 0 )
	: moment().year( 0 ).month( 0 ).date( 1 ).hours( 0 ).minutes( 0 ).seconds( 0 );

export const MAX_DATE = moment().add( 100, "years" )
	.month( 11 ).date( 31 ).hours( 23 ).minutes( 59 ).seconds( 59 );

export const VIEW_MODES = [ "month", "year", "years" ];

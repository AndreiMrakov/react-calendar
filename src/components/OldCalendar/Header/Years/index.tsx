import moment from "moment";
import React from "react";
import Context from "@src/components/OldCalendar/utils/Context.js";

import style from './style.module.scss';

const Years = ( { ...props } ) => {
	return (
		<Context.Consumer>
			{
				( { date,delta } ) =>
					<div className = { style["year-range"] }>
						{
							`${ moment(  ).add( delta * 12 - 4, "years" ).format( "YYYY" ) } –` +
							` ${ moment( ).add( delta * 12 + 7, "years" ).format( "YYYY" ) }`
						}
					</div>

			}
		</Context.Consumer>
	);
};

export default Years;

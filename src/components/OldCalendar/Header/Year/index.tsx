import moment from "moment";
import React from "react";
import Context from "@src/components/OldCalendar/utils/Context.js";

import style from './style.module.scss';

const Year = ( { ...props } ) => {

	return (
		<Context.Consumer>
			{
				( { date, setDateAndPeriod } ) => {
					const handleSetYearsMode = () => setDateAndPeriod( { period: "years" } );
					return <div className = { style["year"] } onClick = { handleSetYearsMode }>
						{ moment( date ).format( "YYYY" ) }
					</div>;
				}
			}
		</Context.Consumer>
	);
};

export default Year;

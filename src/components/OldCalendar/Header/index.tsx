import React from "react";
import Month from "@src/components/OldCalendar/Header/Month";
import Year from "@src/components/OldCalendar/Header/Year";
import Years from "@src/components/OldCalendar/Header/Years";

import style from './style.module.scss';

const Header = ( { period, ...props } ) => {

	const modeReturner = mode => {
		switch ( mode ) {
			case "year":
				return <Year />;
			case "years":
				return <Years />;
			default :
				return <Month />;
		}
	};
	return (
		<div className = { style["header-container"] }>
			{
				modeReturner( period )
			}
		</div>
	);
};
export default Header;

import moment from "moment";
import React from "react";
import Context from "@src/components/OldCalendar/utils/Context";

import style from './style.module.scss';

const Month = ( { ...props } ) => {
	return (
		<Context.Consumer>
			{
				( { date, setDateAndPeriod } ) => {
					const handleSetYearsMode = () => setDateAndPeriod( { period: "year" } );
					return (
						<div className = { style["month"] } onClick = { handleSetYearsMode }>
							{ moment( date ).format( "MMM YYYY" ) }
						</div>
					);
				}
			}
		</Context.Consumer>
	);
};

export default Month;

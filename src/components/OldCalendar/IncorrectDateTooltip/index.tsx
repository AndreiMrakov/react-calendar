import React from "react";
import Popup from "reactjs-popup";

import style from './style.module.scss';

const IncorrectDateTooltip = props => {
	return (
		<Popup
			open = { props.open }
			trigger = { props.trigger }
			contentStyle = { {
				width: "256px",
				background: "#838997",
				borderRadius: "8px",
				padding: "12px 20px",
				zIndex: "15",
				border: "none",
			} }
			arrowStyle = { { background: "#838997" } }
			position = { "top center" }
			modal = { false }
			keepTooltipInside = { true }
			closeOnDocumentClick = { false }
			closeOnEscape = { false }
			repositionOnResize = { false }
			overlayStyle = { { display: "none", pointerEvents: "none" } }
			on = { "" }
		>
			<div className = { style["error-date-tooltip"] }>
				<div className = { style["incorrect"] }>
					<p className = { style["text"] }>
						{
							props.end
								? "End date can’t be earlier than the Start date."
								: "Start date can’t be later than the End date."
						}
					</p>
				</div>
			</div>
		</Popup>
	);
};

export default IncorrectDateTooltip;

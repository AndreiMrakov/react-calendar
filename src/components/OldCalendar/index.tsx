import moment from 'moment';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Body from './Body';
import Header from './Header';
import style from './style.module.scss';
import { MAX_DATE, MIN_DATE, VIEW_MODES } from './utils/constants';
import Context from './utils/Context';

class Calendar extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			currentDate: moment().format(),
			currentPeriod: 'month',
			selectedDate: null,
			delta: 0,
			validRange: {
				// start: "2000-01-01T17:13:41+03:00",
				// end: "2029-12-31T23:59:41+03:00",
			},
			hoveredDate: {},
			selectedHoveredDate: null,
			timer: null,
		};
	}

	deltaFounder = date => {
		if ( moment( date ).isValid() ) {
			let yearsBetween = Math.ceil( moment( date ).diff( moment(), 'years', true ) );
			if ( yearsBetween > 7 ) {
				if ( yearsBetween / 12 >= 1 ) {
					if ( yearsBetween % 12 > 7 ) {
						return Math.trunc( yearsBetween / 12 ) + 1;
					} else return Math.trunc( yearsBetween / 12 );
				} else return 1;
			} else if ( yearsBetween < -4 ) {
				if ( Math.abs( yearsBetween / 12 ) >= 1 ) {
					if ( Math.abs( yearsBetween % 12 ) > 4 ) {
						return Math.trunc( yearsBetween / 12 ) - 1;
					} else return Math.trunc( yearsBetween / 12 );
				} else return -1;
			}
		}
		return 0;
	};

	componentDidMount() {
		let { minDate, maxDate, date } = this.props;
		if ( date && moment( date ).isValid() ) {
			this.setState( {
				currentDate: date,
				selectedDate: date,
				delta: this.deltaFounder( date ),
			} );
		}
		if ( minDate && moment( minDate ).isValid() && maxDate && moment( maxDate ).isValid() ) {
			this.setState( {
				validRange: {
					start: minDate,
					end: maxDate,
				},
			} );
		} else {
			if ( minDate && moment( minDate ).isValid() ) {
				this.setState( { validRange: { start: minDate } } );
			} else if ( maxDate && moment( maxDate ).isValid() ) {
				this.setState( { validRange: { end: maxDate } } );
			}
		}
	}

	componentDidUpdate( prevProps, prevState ) {
		if ( prevState.currentDate !== this.state.currentDate ) {
			let newDelta = this.deltaFounder( this.state.currentDate );
			if ( prevState.delta !== newDelta ) {
				this.setState( { delta: newDelta } );
			}
		}
	}

	componentWillUnmount() {
		if(this.state.timer){
			clearTimeout(this.state.timer);
		}
	}

	handleSetDateAndPeriod = ( { date, period } ) => {
		if ( this.state.timer ) {
			clearTimeout( this.state.timer );
		}
		if (
			date && moment( date ).isValid() && moment( date ).isBetween( MIN_DATE, MAX_DATE, '[]' )
			&& period && VIEW_MODES.includes( period )
		) {
			this.setState( {
				currentDate: date,
				currentPeriod: period,
				hoveredDate: {},
				selectedHoveredDate: null,
				timer: null,
			} );
		} else if ( period && VIEW_MODES.includes( period ) ) {
			this.setState( {
				currentPeriod: period,
				hoveredDate: {},
				selectedHoveredDate: null,
			} );
		} else if ( date && moment( date ).isValid() && moment( date ).isBetween( MIN_DATE, MAX_DATE, '[]' ) ) {
			this.setState( {
				currentDate: date,
				hoveredDate: {},
				selectedHoveredDate: null,
			} );
		}
	};
	handleSelectDate = newSelectedDate => {
		if ( newSelectedDate && moment( newSelectedDate ).isValid() ) {
			this.setState( { selectedDate: newSelectedDate }, () => {
				if ( typeof this.props.onChange === 'function' ) {
					this.props.onChange( newSelectedDate );
				}
			} );
		} else this.setState( { selectedDate: null } );
	};

	handleSelectSelectedHoveredDate = newDate => {
		if ( this.state.timer ) {
			clearTimeout( this.state.timer );
		}
		if ( newDate && moment( newDate ).isValid() ) {
			let timerId = setTimeout( () => this.setState( { selectedHoveredDate: null } ), 4000 );
			this.setState( { selectedHoveredDate: newDate, timer: timerId } );
		} else this.setState( { selectedHoveredDate: null, timer: null } );
	};

	handleSetHoveredDate = ( newHoveredDate, rangeToCompare ) => {
		if ( newHoveredDate && moment( newHoveredDate ).isValid() ) {
			let { start, end } = this.state.validRange;
			if (
				( !start && !end )
				|| ( start && !end && moment( newHoveredDate ).isSameOrAfter( moment( start ), rangeToCompare ) )
				|| ( !start && end && moment( newHoveredDate ).isSameOrBefore( moment( end ), rangeToCompare ) )
				|| moment( newHoveredDate ).isBetween( start, end )
			) {
				this.setState( { hoveredDate: { valid: true, value: newHoveredDate } } );
			} else this.setState( { hoveredDate: { valid: false, value: newHoveredDate } } );
		} else this.setState( { hoveredDate: {} } );
	};

	handleOverlayClick = e => {
		e.stopPropagation();
		this.handleSelectSelectedHoveredDate();
	};

	render() {
		let { customHeader, classNames, customFooter } = this.props;
		let { currentPeriod } = this.state;
		return (
			<Context.Provider
				value = { {
					period: currentPeriod,
					date: this.state.currentDate,
					selectedDate: this.state.selectedDate,
					delta: this.state.delta,
					hoveredDate: this.state.hoveredDate,
					selectDate: this.handleSelectDate,
					setDateAndPeriod: this.handleSetDateAndPeriod,
					setHoverDate: this.handleSetHoveredDate,
					setSelectedHoveredDate: this.handleSelectSelectedHoveredDate,
					selectedHoveredDate: this.state.selectedHoveredDate,
					invalidPopup: this.props.invalidPopup,
				} }
			>
				<div className = { classNames && classNames.container || style.container } onClick = { this.handleOverlayClick }>
					{
						customHeader && customHeader.props.children
							? Array.isArray( customHeader.props.children )
							? <div className = { 'header-container' }>{ <customHeader period = { currentPeriod } /> }</div>
							: <customHeader period = { currentPeriod } />
							: <Header period = { currentPeriod } />
					}
					<Body period = { currentPeriod } />
					{/*{
					 customFooter && customFooter.props.children
					 ? Array.isArray( customFooter.props.children )
					 ? <div className = { "footer-container" }>{ <customFooter period = { currentPeriod } date = { currentDate } /> }</div>
					 : <customFooter period = { currentPeriod } date = { currentDate } />
					 : <Footer period = { currentPeriod } date = { currentDate } />
					 }*/ }
				</div>
			</Context.Provider>
		);
	}
}

export default Calendar;

# React Typescript Template

React template with typescript config with redux-thunk 

### RULES:
1. Insert `.svg` images:
    1. import `Image` from assets into the variable (should starts with capital letter);
    2. import `SVG` component from `react-inlinesvg` library;
    3. insert `Image` variable as `src` attribute into `SVG` component.
    - [x] **EXAMPLE:** 
    ```javascript
       import React from 'react';
       import SVG from 'react-inlinesvg';
       import Image from '@images/image.svg';

       export const SomeImage = () => <SVG src={ Image } />;
    ```
